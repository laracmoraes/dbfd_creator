# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0004_module_parent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='eer_diagram',
            field=models.ForeignKey(to='dbfd.EerDiagram', null=True),
        ),
    ]
