# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0010_configuration'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subclass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('entity_type', models.ForeignKey(related_name='subclass', to='dbfd.EntityType')),
            ],
        ),
        migrations.CreateModel(
            name='Superclass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.RenameField(
            model_name='category',
            old_name='final_entity',
            new_name='category',
        ),
        migrations.RenameField(
            model_name='specialization',
            old_name='initial_entity',
            new_name='superclass',
        ),
        migrations.RemoveField(
            model_name='category',
            name='initial_entity',
        ),
        migrations.RemoveField(
            model_name='category',
            name='participation',
        ),
        migrations.RemoveField(
            model_name='specialization',
            name='final_entity',
        ),
        migrations.AddField(
            model_name='superclass',
            name='category',
            field=models.ForeignKey(to='dbfd.Category'),
        ),
        migrations.AddField(
            model_name='superclass',
            name='entity_type',
            field=models.ForeignKey(related_name='superclasses', to='dbfd.EntityType'),
        ),
        migrations.AddField(
            model_name='superclass',
            name='participation',
            field=models.ForeignKey(to='dbfd.Participation', null=True),
        ),
        migrations.AddField(
            model_name='subclass',
            name='specialization',
            field=models.ForeignKey(to='dbfd.Specialization'),
        ),
    ]
