# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0012_directinheritance'),
    ]

    operations = [
        migrations.AddField(
            model_name='associativeentity',
            name='eer_diagram',
            field=models.ForeignKey(default=0, to='dbfd.EerDiagram'),
        ),
        migrations.AddField(
            model_name='directinheritance',
            name='eer_diagram',
            field=models.ForeignKey(default=0, to='dbfd.EerDiagram'),
        ),
    ]
