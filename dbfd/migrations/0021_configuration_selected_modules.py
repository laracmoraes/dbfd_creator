# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0020_auto_20151023_1303'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='selected_modules',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
