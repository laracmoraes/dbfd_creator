# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0023_module_dbfd'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='dbfd',
            field=models.ForeignKey(related_name='document_dbfd', default='', to='dbfd.DBFD'),
            preserve_default=False,
        ),
    ]
