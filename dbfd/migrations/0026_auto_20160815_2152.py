# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0025_auto_20160607_2203'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuration',
            name='dbfd',
            field=models.ForeignKey(related_name='configuration_dbfd', default=0, to='dbfd.DBFD'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='constraint',
            name='label',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='module',
            name='name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='relation',
            name='label',
            field=models.CharField(max_length=100),
        ),
    ]
