# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0005_auto_20150913_2157'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='abstract',
            field=models.BooleanField(default=False),
        ),
    ]
