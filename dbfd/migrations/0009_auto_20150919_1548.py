# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0008_eerdiagram_docfile'),
    ]

    operations = [
        migrations.CreateModel(
            name='Annotation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=300)),
                ('constraint', models.ForeignKey(to='dbfd.Constraint', blank=True)),
                ('relation', models.ForeignKey(to='dbfd.Relation', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AssociativeEntity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.RemoveField(
            model_name='eerdiagram',
            name='diagram_id',
        ),
        migrations.RemoveField(
            model_name='module',
            name='eer_diagram',
        ),
        migrations.AddField(
            model_name='category',
            name='participation',
            field=models.ForeignKey(to='dbfd.Participation', null=True),
        ),
        migrations.AddField(
            model_name='eerdiagram',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, default=0, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='eerdiagram',
            name='module_id',
            field=models.ForeignKey(default=b'', to='dbfd.Module'),
        ),
        migrations.AddField(
            model_name='relationshiptype',
            name='is_identifier',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='specialization',
            name='disjoint',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='specialization',
            name='participation',
            field=models.ForeignKey(to='dbfd.Participation', null=True),
        ),
        migrations.AlterField(
            model_name='eerdiagram',
            name='docfile',
            field=models.FileField(upload_to=b'documents'),
        ),
        migrations.AddField(
            model_name='associativeentity',
            name='relationship',
            field=models.ForeignKey(to='dbfd.RelationshipType'),
        ),
    ]
