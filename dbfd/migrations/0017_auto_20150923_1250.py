# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0016_compositeattribute'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConstraintAnnotation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=300)),
                ('constraint', models.ForeignKey(to='dbfd.Constraint', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='RelationAnnotation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.CharField(max_length=300)),
                ('relation', models.ForeignKey(to='dbfd.Relation', blank=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='annotation',
            name='constraint',
        ),
        migrations.RemoveField(
            model_name='annotation',
            name='relation',
        ),
        migrations.DeleteModel(
            name='Annotation',
        ),
    ]
