# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0003_auto_20150910_0844'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='parent',
            field=models.ForeignKey(related_name='parent_module', to='dbfd.Module', null=True),
        ),
    ]
