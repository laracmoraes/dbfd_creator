# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0013_auto_20150922_2242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attribute',
            name='entity_type',
            field=models.ForeignKey(blank=True, to='dbfd.EntityType', null=True),
        ),
        migrations.AlterField(
            model_name='attribute',
            name='relationship_type',
            field=models.ForeignKey(blank=True, to='dbfd.RelationshipType', null=True),
        ),
    ]
