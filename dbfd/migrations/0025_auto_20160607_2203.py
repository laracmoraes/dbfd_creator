# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0024_document_dbfd'),
    ]

    operations = [
        migrations.AddField(
            model_name='constraint',
            name='dbfd',
            field=models.ForeignKey(related_name='constraint_dbfd', default='', to='dbfd.DBFD'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='relation',
            name='dbfd',
            field=models.ForeignKey(related_name='relation_dbfd', default='', to='dbfd.DBFD'),
            preserve_default=False,
        ),
    ]
