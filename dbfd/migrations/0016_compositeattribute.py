# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0015_auto_20150923_0902'),
    ]

    operations = [
        migrations.CreateModel(
            name='CompositeAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('attribute_type', models.CharField(max_length=100)),
                ('entity_attribute', models.ForeignKey(to='dbfd.EntityAttribute')),
            ],
        ),
    ]
