# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0007_auto_20150917_1928'),
    ]

    operations = [
        migrations.AddField(
            model_name='eerdiagram',
            name='docfile',
            field=models.FileField(null=True, upload_to=b'documents'),
        ),
    ]
