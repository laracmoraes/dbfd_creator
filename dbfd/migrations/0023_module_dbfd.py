# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0022_dbfd'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='dbfd',
            field=models.ForeignKey(related_name='dbfd', default='', to='dbfd.DBFD'),
            preserve_default=False,
        ),
    ]
