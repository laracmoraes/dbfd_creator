# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0018_auto_20150923_2255'),
    ]

    operations = [
        migrations.AddField(
            model_name='compositeattribute',
            name='size',
            field=models.IntegerField(default=20),
        ),
        migrations.AddField(
            model_name='entityattribute',
            name='size',
            field=models.IntegerField(default=20),
        ),
        migrations.AddField(
            model_name='relationshipattribute',
            name='size',
            field=models.IntegerField(default=20),
        ),
    ]
