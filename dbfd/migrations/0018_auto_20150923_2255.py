# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0017_auto_20150923_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='compositeattribute',
            name='data_type',
            field=models.CharField(default=b'STRING', max_length=30),
        ),
        migrations.AddField(
            model_name='compositeattribute',
            name='is_null',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='compositeattribute',
            name='is_unique',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='entityattribute',
            name='data_type',
            field=models.CharField(default=b'STRING', max_length=30),
        ),
        migrations.AddField(
            model_name='entityattribute',
            name='is_null',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='entityattribute',
            name='is_unique',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='relationshipattribute',
            name='data_type',
            field=models.CharField(default=b'STRING', max_length=30),
        ),
        migrations.AddField(
            model_name='relationshipattribute',
            name='is_null',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='relationshipattribute',
            name='is_unique',
            field=models.BooleanField(default=False),
        ),
    ]
