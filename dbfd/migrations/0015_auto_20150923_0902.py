# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0014_auto_20150922_2247'),
    ]

    operations = [
        migrations.CreateModel(
            name='EntityAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('attribute_type', models.CharField(max_length=100)),
                ('entity_type', models.ForeignKey(to='dbfd.EntityType')),
            ],
        ),
        migrations.CreateModel(
            name='RelationshipAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('attribute_type', models.CharField(max_length=100)),
                ('relationship_type', models.ForeignKey(to='dbfd.RelationshipType')),
            ],
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='entity_type',
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='relationship_type',
        ),
        migrations.DeleteModel(
            name='Attribute',
        ),
    ]
