# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dbfd', '0011_auto_20150922_2108'),
    ]

    operations = [
        migrations.CreateModel(
            name='DirectInheritance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source', models.ForeignKey(related_name='source', to='dbfd.EntityType')),
                ('target', models.ForeignKey(related_name='target', to='dbfd.EntityType')),
            ],
        ),
    ]
