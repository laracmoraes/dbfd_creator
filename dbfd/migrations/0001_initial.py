# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('attribute_type', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Cardinality',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Constraint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ConstraintType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ctype', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='EerDiagram',
            fields=[
                ('diagram_id', models.IntegerField(serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='EntityType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('weak', models.BooleanField(default=False)),
                ('eer_diagram', models.ForeignKey(to='dbfd.EerDiagram')),
            ],
        ),
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('mandatory', models.BooleanField(default=True)),
                ('eer_diagram', models.ForeignKey(to='dbfd.EerDiagram')),
            ],
        ),
        migrations.CreateModel(
            name='Participation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Relation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100)),
                ('final_module', models.ForeignKey(related_name='module_final', to='dbfd.Module')),
                ('initial_module', models.ForeignKey(related_name='module_initial', to='dbfd.Module')),
            ],
        ),
        migrations.CreateModel(
            name='RelationshipType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('cardinality', models.ForeignKey(to='dbfd.Cardinality')),
                ('eer_diagram', models.ForeignKey(to='dbfd.EerDiagram')),
                ('final_entity', models.ForeignKey(related_name='entity_final', to='dbfd.EntityType')),
                ('initial_entity', models.ForeignKey(related_name='entity_initial', to='dbfd.EntityType')),
                ('participation', models.ForeignKey(to='dbfd.Participation')),
            ],
        ),
        migrations.CreateModel(
            name='RelationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rtype', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Specialization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('eer_diagram', models.ForeignKey(to='dbfd.EerDiagram')),
                ('final_entity', models.ForeignKey(related_name='subclass', to='dbfd.EntityType')),
                ('initial_entity', models.ForeignKey(related_name='superclass', to='dbfd.EntityType')),
            ],
        ),
        migrations.AddField(
            model_name='relation',
            name='relation_type',
            field=models.ForeignKey(to='dbfd.RelationType'),
        ),
        migrations.AddField(
            model_name='constraint',
            name='constraint_type',
            field=models.ForeignKey(to='dbfd.ConstraintType'),
        ),
        migrations.AddField(
            model_name='constraint',
            name='final_module',
            field=models.ForeignKey(related_name='moduleB', to='dbfd.Module'),
        ),
        migrations.AddField(
            model_name='constraint',
            name='initial_module',
            field=models.ForeignKey(related_name='moduleA', to='dbfd.Module'),
        ),
        migrations.AddField(
            model_name='category',
            name='eer_diagram',
            field=models.ForeignKey(to='dbfd.EerDiagram'),
        ),
        migrations.AddField(
            model_name='category',
            name='final_entity',
            field=models.ForeignKey(related_name='category', to='dbfd.EntityType'),
        ),
        migrations.AddField(
            model_name='category',
            name='initial_entity',
            field=models.ForeignKey(related_name='subclassA', to='dbfd.EntityType'),
        ),
        migrations.AddField(
            model_name='attribute',
            name='entity_type',
            field=models.ForeignKey(to='dbfd.EntityType', blank=True),
        ),
        migrations.AddField(
            model_name='attribute',
            name='relationship_type',
            field=models.ForeignKey(to='dbfd.RelationshipType', blank=True),
        ),
    ]
