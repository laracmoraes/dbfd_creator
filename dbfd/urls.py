from django.conf.urls import patterns, url
from . import views

urlpatterns = [
    #urls referentes a usuarios
    url(r'^$', views.home, name='home'),	
    url(r'^login/$', views.login_user, name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^register/$', views.register, name='register'),
    url(r'^password_change/$', views.password_change, name='password_change'),
    # url(r'^users/$', views.user_list, name='user_list'),

    url(r'^diagram/list/$', views.list_diagrams, name='diagram_list'),
    #url para criar feature diagram (incluir import file e gerenciar modulos pela pagina)
    url(r'^diagram/(?P<dbfd_id>\d+)/create/(?P<tab>\d+)/$', views.upload_diagram, name='upload'),
    #url(r'^diagram/create/$', views.upload_diagram, name='upload'),
    # url(r'^module/(?P<id>\d+)/delete$', views.delete_module, name='delete_module'),

    #urls para criar e deletar diagramas eer para modulos
    url(r'^eer_diagram/upload/(?P<module_id>\d+)$', views.upload_eer_diagram, name='upload_eer'),
    # url(r'^eer_diagram/delete/(?P<module_id>\d+)$', views.delete_eer_diagram, name='delete_eer'),
    
    #url para criar nova restricao na pagina (ja existem as importadas pelo arquivo do feature diagram)
    url(r'^constraint/(?P<id>\d+)/delete$', views.delete_constraint, name='delete_constraint'),

    #url para criar e deletar anotacoes para relacoes e restricoes
    url(r'^annotation/(?P<atype>\w+)/(?P<id>\d+)/(?P<action>\w+)$', views.create_annotation, name='annotation'),
    url(r'^annotation/delete/(?P<atype>\w+)/(?P<id>\d+)$', views.delete_annotation, name='delete_annotation'),

    #url para criar e validar uma configuracao
    url(r'^configuration/(?P<id>\d+)/create$', views.create_configuration, name='create_configuration'),
    
    #url para criar o script sql para uma configuracao (script inicial ou de atualizacao)
    url(r'^sql_script/$', views.generate_new_sql_script, name='sql_script'),

    #url para as funcoes prontas de atualizar o diagrama para o neurocientista
    url(r'^diagram/update/$', views.update_diagram, name='update_diagram'),
    
    #url para download de arquivos
    url(r'^download/(?P<file_name>.+)$', views.download, name='download'),
]