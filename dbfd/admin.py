from django.contrib import admin

from .models import Cardinality
from .models import Participation
from .models import RelationType
from .models import ConstraintType
from .models import Module
from .models import Relation
from .models import Constraint
from .models import Document
from .models import EerDiagram
from .models import Configuration
from .models import EntityType
from .models import RelationshipType
from .models import AssociativeEntity
from .models import EntityAttribute
from .models import RelationshipAttribute
from .models import Specialization
from .models import DirectInheritance
from .models import Category
from .models import Subclass
from .models import Superclass
from .models import CompositeAttribute
from .models import RelationAnnotation
from .models import ConstraintAnnotation
from .models import DBFD

admin.site.register(Cardinality)
admin.site.register(Participation)
admin.site.register(RelationType)
admin.site.register(ConstraintType)
admin.site.register(Module)
admin.site.register(Relation)
admin.site.register(Constraint)
admin.site.register(Document)
admin.site.register(EerDiagram)
admin.site.register(Configuration)
admin.site.register(EntityType)
admin.site.register(RelationshipType)
admin.site.register(AssociativeEntity)
admin.site.register(EntityAttribute)
admin.site.register(RelationshipAttribute)
admin.site.register(Specialization)
admin.site.register(DirectInheritance)
admin.site.register(Category)
admin.site.register(Subclass)
admin.site.register(Superclass)
admin.site.register(CompositeAttribute)
admin.site.register(RelationAnnotation)
admin.site.register(ConstraintAnnotation)
admin.site.register(DBFD)