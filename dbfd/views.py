from django.shortcuts import render_to_response, render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from .models import Document, Module, Relation, Constraint, ConstraintType, RelationType, EerDiagram, Configuration, EntityType, RelationshipType, AssociativeEntity, EntityAttribute, RelationshipAttribute, Specialization, Category, Subclass, Superclass, DirectInheritance, Cardinality, Participation, CompositeAttribute, RelationAnnotation, ConstraintAnnotation, DBFD
from .forms import DocumentForm, UserForm
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
import xml.etree.ElementTree as ET
from xml.dom.minidom import parse
import xml.dom.minidom
from django.conf import settings
from wsgiref.util import FileWrapper
import mimetypes
import os
from django.utils.encoding import smart_str
import pydot
import time
from django.core.files import File
import utils

@login_required
def home(request, template_name='index.html'):
    return render(request, template_name)

def register(request, template_name='register.html'):
    context = RequestContext(request)
    registered = False
    groups = Group.objects.all()
    if request.method == 'POST':
        user_form = UserForm(request.POST or None)
        group = request.POST['group'] 
        if user_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()
            g = Group.objects.get(id=group) 
            g.user_set.add(user)
            registered = True
        else:
            print user_form.errors

    else:
        user_form = UserForm()

    return render_to_response(
            template_name,
            {'user_form': user_form, 'registered': registered, 'groups':groups}, context)

def login_user(request, template_name='auth.html'):
    state = "Please log in below..."
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        context = RequestContext(request)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('/dbfd/')
            else:
                state = "Your account is not active, please contact the site admin."
        else:
            state = "Your username and/or password were incorrect."

    return render(request, template_name, {'state':state, 'username': username})

def logout_user(request):
    logout(request)
    return render(request, 'auth.html', {'state':"You are logged out.", 'username': ''})

def password_change(request):
    context = RequestContext(request)
    new_password = ''
    if request.POST:
        if 'change_password' in request.POST:
            new_password = request.POST.get('password')
            u = User.objects.get(username__exact=request.user.username)
            u.set_password(new_password)
            u.save()

    groups = Group.objects.all()
    user_groups = request.user.groups.all()
    if user_groups:
        current_group = user_groups[0]
    else:
        current_group = None

    return render(request, 'password_change.html',
        {'username': request.user.username, 'current_group': current_group, 'groups': groups})

@login_required
def list_diagrams(request):
    message = ''
    message_type = ''
    if request.method == 'POST':
        dbfd_name = request.POST['dbfd_name']
        if dbfd_name:
            try:
                DBFD.objects.get(name=dbfd_name)
                message = 'This DBFD name already exists.'
                message_type = 'danger'
            except DBFD.DoesNotExist:
                dbfd = DBFD(name=dbfd_name)
                dbfd.save()
                root = Module(name=dbfd_name, mandatory=True, abstract=True, dbfd=dbfd)
                root.save()
                message = 'DBFD added with success!'  
                message_type = 'success'

    dbfds = DBFD.objects.all()
    return render_to_response(
        'dbfd_list.html', {'dbfds': dbfds, 'message': message, 'message_type': message_type},
        context_instance=RequestContext(request)
    )

@login_required
def upload_diagram(request, dbfd_id, tab):
    message = ''
    message_type = 'info'
    dbfd = DBFD.objects.get(id=dbfd_id)
    if request.method == 'POST':
        if 'new_module' in request.POST:
            module_name = request.POST['module_name'].replace(" ","")
            parent = request.POST['parent']
            rel_type = request.POST['rel_type']
            parent_module = Module.objects.get(name=parent, dbfd=dbfd)
            abstract = request.POST['abstract']
            if module_name and parent and rel_type:
                try:
                    Module.objects.get(name=module_name, dbfd=dbfd)
                    message = 'This module name already exists.'
                    message_type = 'danger'
                except Module.DoesNotExist:
                    if rel_type == 'mandatory':
                        module = Module(name=module_name, mandatory=True, abstract=abstract, parent=parent_module, dbfd=dbfd)
                    else:
                        module = Module(name=module_name, mandatory=False, abstract=abstract, parent=parent_module, dbfd=dbfd)
                    module.save()
                    if rel_type == 'mandatory' or rel_type == 'optional':
                        rel_type = 'and'
                    rtype = RelationType.objects.get(rtype=rel_type)
                    relation = Relation(label="R ("+parent+", "+module_name+")", initial_module=parent_module, final_module=module, relation_type=rtype, dbfd=dbfd)
                    relation.save()
                    message = 'Module added with success!'  
                    message_type = 'success'  
        elif 'new_constraint' in request.POST:
            initial = request.POST['initial']
            final = request.POST['final']
            cons_type = request.POST['cons_type']
            initial_module = Module.objects.get(name=initial, dbfd=dbfd)
            final_module = Module.objects.get(name=final, dbfd=dbfd)
            if cons_type == 'requires':
                rel_initial = Relation.objects.filter(initial_module=initial_module.parent, final_module=initial_module)
                rel_final = Relation.objects.filter(initial_module=final_module.parent, final_module=final_module)
                if rel_initial[0].relation_type == rel_final[0].relation_type and rel_initial[0].relation_type.rtype == 'alternative':
                    message = 'You can\'t create requires constraint with two alternative modules'
                    message_type = 'danger'
                else:
                    if initial and final and cons_type:
                        ctype = ConstraintType.objects.get(ctype=cons_type)
                        cons_label = "C ("+initial+", "+final+")"
                        try:
                            Constraint.objects.get(label=cons_label)
                            message = 'This constraint name already exists.'
                            message_type = 'danger'
                        except Constraint.DoesNotExist:
                            constraint = Constraint(label=cons_label, initial_module=initial_module, final_module=final_module, constraint_type=ctype, dbfd=dbfd)
                            constraint.save()
                            message = 'Constraint included with success!'    
                            message_type = 'success'
            else:
                if initial_module.mandatory or final_module.mandatory:
                    message = 'This excludes constraint makes the model invalid!'
                    message_type = 'danger'
                else:
                    if initial and final and cons_type:
                        ctype = ConstraintType.objects.get(ctype=cons_type)
                        cons_label = "C ("+initial+", "+final+")"
                        try:
                            Constraint.objects.get(label=cons_label)
                            message = 'This constraint name already exists.'
                            message_type = 'danger'
                        except Constraint.DoesNotExist:
                            constraint = Constraint(label=cons_label, initial_module=initial_module, final_module=final_module, constraint_type=ctype, dbfd=dbfd)
                            constraint.save()
                            message = 'Constraint included with success!'  
                            message_type = 'success'  
        elif 'delete_module' in request.POST:
            deleted_module = Module.objects.filter(name=request.POST['del_module'])
            children = Module.objects.filter(parent=deleted_module)
            if children:
                message_type = "danger"
                message = "You can't delete a module with children modules!"
            else:
                deleted_module.delete()
        else:
            documents = Document.objects.filter(dbfd=dbfd)
            if documents:
                modules = Module.objects.filter(dbfd=dbfd, parent__isnull=False)
                relations = Relation.objects.filter(dbfd=dbfd)
                constraints = Constraint.objects.filter(dbfd=dbfd)
                documents.delete()
                modules.delete()
                relations.delete()
                constraints.delete()
            form = DocumentForm(request.POST, request.FILES)
            if form.is_valid():
                newdoc = Document(docfile = request.FILES['docfile'], dbfd=dbfd)
                newdoc.save()

                handle_uploaded_file(request.FILES['docfile'], dbfd_id)
                tab = 1
                return HttpResponseRedirect(reverse('upload', args=(dbfd_id, tab,)))
            else:
                message = 'Something went wrong when uploading diagram file'  
                message_type = 'danger' 

    form = DocumentForm()

    documents = Document.objects.filter(dbfd=dbfd)
    configuration = Configuration.objects.filter(dbfd=dbfd)
    modules = Module.objects.filter(dbfd=dbfd)
    relations = Relation.objects.filter(dbfd=dbfd)
    constraints = Constraint.objects.filter(dbfd=dbfd)
    module_list = []
    eer_diagrams = False
    hierarchy = []
    node_id = 0
    if modules:
        eer_diagrams = True
    for module in modules:
        eer_diagram = EerDiagram.objects.filter(module_id=module)
        if eer_diagram:
            has_eer = True
        else:
            if module.abstract:
                has_eer = 'abstract'
            else:
                has_eer = False
                eer_diagrams = False
        module_type = ''
        if module.parent is None:
            module_type = 'mandatory'
        else:
            parental_relation = Relation.objects.filter(initial_module=module.parent, final_module=module)
            if parental_relation[0].relation_type.rtype == 'and':
                if module.mandatory:
                    module_type = 'mandatory'
                else:
                    module_type = 'optional'
            else:
                if parental_relation[0].relation_type.rtype == 'or':
                    module_type = 'or'
                else:
                    module_type = 'alternative'
        if module.parent is None:
            module_list.append({ "module": module, "type": module_type, "level": node_id, "parent_level": 0, "padding": 0, 'has_eer': has_eer})
            hierarchy.append({ "name": module.name, "level": node_id, "child": 1, "padding": 0})
        else:
            for h in hierarchy:
                parent_name = module.parent.name
                if h["name"] == parent_name:
                    parent_id = h["level"]
                    parent_padding = h["padding"]
                    child_level = h["child"]
            module_list.insert(child_level, {"module": module, "type": module_type, "level": child_level, "parent_level": parent_id, "padding": 19*(parent_padding+1), 'has_eer': has_eer})
            hierarchy.append({ "name": module.name, "level": child_level, "child": child_level + 1, "padding": parent_padding+1})
            parental_list = []
            parental_list.append(module.parent)
            for parent in parental_list:
                for element in hierarchy:
                    if element['name'] == parent.name:
                        element['child'] = element['child'] + 1
                grand_parent = parent.parent
                if grand_parent:
                    parental_list.append(grand_parent)
            if child_level < node_id:
                for element in hierarchy:
                    if element['level'] >= child_level and element['name'] != module.name:
                        element['level'] = element['level'] + 1
                        element['child'] = element['child'] + 1
                for element in module_list:
                    element['level'] = module_list.index(element)                   
                    if element['parent_level'] >= child_level and element['module'] != module.name:
                        element['parent_level'] = element['parent_level'] + 1
        node_id += 1                

    annotations = False
    if relations or constraints:
        annotations = True
    for relation in relations:
        rel_annotation = RelationAnnotation.objects.filter(relation=relation)
        if not rel_annotation:
            annotations = False
    for constraint in constraints:
        cons_annotation = ConstraintAnnotation.objects.filter(constraint=constraint)
        if not cons_annotation:
            annotations = False

    if eer_diagrams:
        valid = True
    else:
        valid = False

    return render_to_response(
        'create_diagram.html',
        {'documents': documents, 'form': form, 'modules': module_list, 'eer_diagrams': eer_diagrams,
        'annotations': annotations, 'relations': relations, 'constraints': constraints, 'valid': valid,
        'message': message, 'message_type': message_type, 'tab': tab, 'dbfd_id': dbfd_id, 'config': configuration},
        context_instance=RequestContext(request)
    )

def handle_uploaded_file(f, dbfd_id):
    parent_module = []
    relation_list = []
    composition = 0

    dbfd = DBFD.objects.get(id=dbfd_id)

    for chunk in f.chunks():
        for line in chunk.splitlines():
            line = line.strip()
            if line.startswith("concept"):
                f = line.split('concept')
                relation_list.append('and')
                parent_module.append(dbfd.name)
            else:
                if line.startswith("constraint"):
                    f = line.strip('constraint').strip(';')
                    if '!' in f:
                        cons = f.split(' -> ! ')
                        ctype = ConstraintType.objects.get(ctype="excludes")  
                        initial = Module.objects.get(name=cons[0].strip(), dbfd=dbfd) 
                        final = Module.objects.get(name=cons[1].strip(), dbfd=dbfd)
                        constraint = Constraint(label="C ("+cons[0].strip()+", "+cons[1].strip()+")", initial_module=initial, final_module=final, constraint_type=ctype, dbfd=dbfd)                 
                        constraint.save()
                    else:
                        cons = f.split(' -> ')
                        ctype = ConstraintType.objects.get(ctype="requires")   
                        initial = Module.objects.get(name=cons[0].strip(), dbfd=dbfd) 
                        final = Module.objects.get(name=cons[1].strip(), dbfd=dbfd)
                        constraint = Constraint(label="C ("+cons[0].strip()+", "+cons[1].strip()+")", initial_module=initial, final_module=final, constraint_type=ctype, dbfd=dbfd)                 
                        constraint.save()
                else:
                    if line.startswith("feature") or line.startswith("mandatory") or line.startswith("abstract"):
                        if line.startswith("mandatory"):
                            f_type = 'mandatory'
                        else:
                            if line.startswith("feature"):
                                f_type = 'optional'
                            else:
                                if line.startswith("abstract"):
                                    f_type = 'abstract'
                        if line.endswith(";"):
                            f = line.split("feature")
                            parent = Module.objects.get(name=parent_module[len(parent_module)-1], dbfd=dbfd)
                            module = Module(name=f[1].strip(';').strip(), mandatory=(f_type=='mandatory'), abstract=(f_type=='abstract'), parent=parent, dbfd=dbfd)
                            module.save()
                            rtype = RelationType.objects.get(rtype=relation_list[len(relation_list)-1])
                            relation = Relation(label="R ("+parent.name+", "+f[1].strip(";").strip()+")", initial_module=parent, final_module=module, relation_type=rtype, dbfd=dbfd)
                            relation.save()
                        else:
                            if line.endswith("{"):
                                f = line.split("feature")
                                parent = Module.objects.get(name=parent_module[len(parent_module)-1], dbfd=dbfd)
                                module = Module(name=f[1].strip('{').strip(), mandatory=(f_type=='mandatory'), abstract=(f_type=='abstract'), parent=parent, dbfd=dbfd)
                                module.save()
                                rtype = RelationType.objects.get(rtype=relation_list[len(relation_list)-1])
                                relation = Relation(label="R ("+parent.name+", "+f[1].strip("{").strip()+")", initial_module=parent, final_module=module, relation_type=rtype, dbfd=dbfd)
                                relation.save()
                                parent_module.append(f[1].strip("{").strip())
                                relation_list.append('and')
                    else:
                        if line.startswith("someOf"):
                            relation_list.append('or')
                        else:
                            if line.startswith("oneOf"):
                                relation_list.append('alternative')
                            else:
                                if line.startswith("}"):
                                    if composition == 0:
                                        if len(relation_list) != 0:
                                            relation_list.pop()
                                            composition = 1
                                    elif composition == 1:
                                        if len(parent_module) != 0:
                                            parent_module.pop()
                                        if len(relation_list) != 0:
                                            relation_list.pop()
                                        composition = 0

@login_required
def upload_eer_diagram(request, module_id):
    module = Module.objects.get(pk=module_id) 
    dbfd_id = module.dbfd.pk
    messages = []
    message_type = 'success'
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = EerDiagram(docfile = request.FILES['docfile'], module_id = module)
            newdoc.save()

            messages = handle_eer_diagram(newdoc.docfile.name, newdoc.pk)
            if not messages:
                tab = 2
                return HttpResponseRedirect(reverse('upload', args=(dbfd_id, tab,)))
    else:
        form = DocumentForm()

    documents = EerDiagram.objects.filter(module_id=module)

    if messages:
        message_type = 'danger'
        eerdiagram = EerDiagram.objects.filter(module_id=module)[0]
        eerdiagram.delete()

    return render_to_response(
        'upload_eer_diagram.html',
        {'documents': documents, 'form': form, 'module': module_id, 'messages': messages, 
        'message_type': message_type, 'dbfd': module.dbfd},
        context_instance=RequestContext(request)
    )

@login_required
def delete_constraint(request, id):
    constraint = Constraint.objects.filter(pk=id)[0]
    dbfd = DBFD.objects.filter(name=constraint.dbfd.name)[0]
    constraint.delete()
    return redirect('upload', dbfd_id=dbfd.pk, tab=1)

def handle_eer_diagram(file_name, eer_id):
    eer_diagram = EerDiagram.objects.filter(pk=eer_id)[0]
    DOMTree = xml.dom.minidom.parse('media/'+file_name.encode('ascii','ignore'))
    collection = DOMTree.documentElement
    nodes = collection.getElementsByTagName("nodes")
    links = collection.getElementsByTagName("links")
    contains = collection.getElementsByTagName("contains")
    entities = []
    relationships = []
    associative_entities = []
    attributes = []
    specializations = []
    categories = []
    direct_inheritances = []
    subclasses = []
    superclasses = []
    messages = []
    for node in nodes:
        ntype = node.getAttributeNS("http://www.omg.org/XMI", "type")
        if ntype == 'eer:Entity':
            isWeak = False
            if node.getAttribute("isWeak"):
                isWeak = True
            entities.append({
                "name": node.getAttribute("name"),
                "weak": isWeak
                })
        if ntype == 'eer:Relationship':
            isIdentifier = False
            if node.getAttribute("isIdentifier"):
                isIdentifier = True
            link = node.getAttribute("relationshipLink").split(" ") 
            source = ''
            target = ''
            for n in nodes:
                if n.getAttributeNS("http://www.omg.org/XMI", "type") == 'eer:Entity':
                    rel_link = n.getAttribute("relationshipLink")
                    if link[0] in rel_link:
                        source = n.getAttribute("name")
                    if link[1] in rel_link:
                        target = n.getAttribute("name")
            for l in links:
                link_id = l.getAttributeNS("http://www.omg.org/XMI", "id")
                link_type = l.getAttributeNS("http://www.omg.org/XMI", "type")
                if str(link_type) == 'eer:RelationshipLink' and link_id == link[0]:
                    source_participation = 'PARTIAL'
                    if l.getAttribute("participation"):
                        source_participation = l.getAttribute("participation")
                    source_cardinality = '1'
                    if l.getAttribute("cardinality"):
                        source_cardinality = 'M'
                if str(link_type) == 'eer:RelationshipLink' and link_id == link[1]:
                    target_participation = 'PARTIAL'
                    if l.getAttribute("participation"):
                        target_participation = l.getAttribute("participation")
                    target_cardinality = '1'
                    if l.getAttribute("cardinality"):
                        target_cardinality = 'N'
            relationships.append({
                "name": node.getAttribute("name"),
                "initial_entity": source,
                "final_entity": target,
                "cardinality": source_cardinality+":"+target_cardinality,
                "participation": source_participation+":"+target_participation,
                "is_identifier": isIdentifier
                })
        if ntype == 'eer:AssociativeEntity':
            associativeRelName = node.getElementsByTagName("contains")[0].getAttribute("name")
            link = node.getElementsByTagName("contains")[0].getAttribute("relationshipLink").split(" ")
            source = ''
            target = ''
            for n in nodes:
                if n.getAttributeNS("http://www.omg.org/XMI", "type") == 'eer:Entity':
                    rel_link = n.getAttribute("relationshipLink")
                    if link[0] in rel_link:
                        source = n.getAttribute("name")
                    if link[1] in rel_link:
                        target = n.getAttribute("name")
            for l in links:
                link_id = l.getAttributeNS("http://www.omg.org/XMI", "id")
                link_type = l.getAttributeNS("http://www.omg.org/XMI", "type")
                if str(link_type) == 'eer:RelationshipLink' and link_id == link[0]:
                    isIdentifier = False
                    if l.getAttribute("isIdentifier"):
                        isIdentifier = True
                    source_participation = 'PARTIAL'
                    if l.getAttribute("participation"):
                        source_participation = l.getAttribute("participation")
                    source_cardinality = '1'
                    if l.getAttribute("cardinality"):
                        source_cardinality = 'M'
                if str(link_type) == 'eer:RelationshipLink' and link_id == link[1]:
                    target_participation = 'PARTIAL'
                    if l.getAttribute("participation"):
                        target_participation = l.getAttribute("participation")
                    target_cardinality = '1'
                    if l.getAttribute("cardinality"):
                        target_cardinality = 'N'
            relationships.append({
                "name": associativeRelName,
                "initial_entity": source,
                "final_entity": target,
                "cardinality": source_cardinality+":"+target_cardinality,
                "participation": source_participation+":"+target_participation,
                "is_identifier": isIdentifier
                })
            associative_entities.append({
                "name": node.getAttribute("name"),
                "relationship": associativeRelName
                })
        if ntype == 'eer:Attribute':
            link = node.getAttribute("AttributeLinkTarget")
            entity_target = ''
            relationship_target = ''
            attribute_target = ''
            attr_type = 'COMMON'
            attr_data_type = 'STRING'
            attr_is_null = False
            attr_is_unique = False
            attr_size = 20
            if node.getAttribute("type"):
                attr_type = node.getAttribute("type")
                if node.getAttribute("dataType"):
                    attr_data_type = node.getAttribute("dataType")
                if node.getAttribute("isNull"):
                    attr_is_null = True
                if node.getAttribute("isUnique"):
                    attr_is_unique = True
                if node.getAttribute("size"):
                    attr_size = node.getAttribute("size")
            for n in nodes:
                link_source = n.getAttribute("AttributeLinkSource").encode('ascii','ignore')
                if link in link_source:
                    if n.getAttributeNS("http://www.omg.org/XMI", "type") == "eer:Entity":
                        entity_target = n.getAttribute("name")
                    else:
                        if n.getAttributeNS("http://www.omg.org/XMI", "type") == "eer:Relationship":
                            relationship_target = n.getAttribute("name")
                        else:
                            if n.getAttributeNS("http://www.omg.org/XMI", "type") == "eer:Attribute":
                                attribute_target = n.getAttribute("name")
            for c in contains:
                link_source = c.getAttribute("AttributeLinkSource").encode('ascii','ignore')
                if link in link_source:
                    relationship_target = c.getAttribute("name")    
            attributes.append({
                "name": node.getAttribute("name"),
                "data_type": attr_data_type,
                "is_null": attr_is_null,
                "is_unique": attr_is_unique,
                "size": attr_size,
                "attribute_type": attr_type,
                "entity_target": entity_target,
                "relationship_target": relationship_target,
                "attribute_target": attribute_target
                })
        if ntype == 'eer:Inheritance':
            disjointness = False
            if node.getAttribute("disjointness"):
                disjointness = True
            subclasses_id = node.getAttribute("inheritanceSL").split(" ") 
            superclass_name = ''
            subclass_name = []
            for n in nodes:
                if n.getAttributeNS("http://www.omg.org/XMI", "type") == 'eer:Entity' and n.getAttribute("inheritanceGL") == node.getAttribute("inheritanceGL"):
                    superclass_name = n.getAttribute("name")
                for sub in subclasses_id:
                    if n.getAttributeNS("http://www.omg.org/XMI", "type") == 'eer:Entity' and sub in n.getAttribute("inheritanceSL"):
                        subclass_name.append(n.getAttribute("name"))
            completeness = 'PARTIAL'
            for l in links:
                if l.getAttributeNS("http://www.omg.org/XMI", "id") == node.getAttribute("inheritanceGL"):
                    if l.getAttribute("completeness"):
                        completeness = l.getAttribute("completeness")
            specializations.append({
                "name": superclass_name+"Spec",
                "superclass": superclass_name,
                "participation": completeness,
                "disjoint": disjointness
                })
            for subclass in subclass_name:
                subclasses.append({
                    "entity": subclass,
                    "specialization": superclass_name+"Spec"
                    })
        if ntype == 'eer:Category':
            superclasses_id = node.getAttribute("categoryGL").split(" ") 
            category_id = node.getAttribute("categorySL")
            category_name = ''
            superclass_name = []
            completeness = []
            for n in nodes:
                if n.getAttributeNS("http://www.omg.org/XMI", "type") == 'eer:Entity' and category_id in n.getAttribute("categorySL"):
                    category_name = n.getAttribute("name")
                if n.getAttributeNS("http://www.omg.org/XMI", "type") == 'eer:Entity' and n.getAttribute("categoryGL"):
                    for sub in superclasses_id:
                        if sub in n.getAttribute("categoryGL"):
                            superclass_name.append(n.getAttribute("name"))
                            for l in links:
                                if l.getAttributeNS("http://www.omg.org/XMI", "type") == "eer:CategoryGL" and l.getAttributeNS("http://www.omg.org/XMI", "id") == sub:
                                    if l.getAttribute("completeness"):
                                        completeness.append(l.getAttribute("completeness"))
                                    else:
                                        completeness.append("PARTIAL")
            categories.append({
                "name": category_name+"Cat",
                "category": category_name
                })
            i=0
            for superclass in superclass_name:
                superclasses.append({
                    "entity": superclass,
                    "category": category_name+"Cat",
                    "participation": completeness[i]
                    })
                i +=1
        if ntype == 'eer:Entity' and node.getAttribute("directInheritanceLinkTarget"):
            for n in nodes:
                if n.getAttribute("directInheritanceLinkSource") == node.getAttribute("directInheritanceLinkTarget"):
                    direct_inheritances.append({
                        "source": n.getAttribute("name"),
                        "target": node.getAttribute("name")
                        })
    for entity in entities:
        try:
            EntityType.objects.get(name=entity.get('name'), eer_diagram=eer_diagram)
            messages.append('Entity name '+entity.get('name')+' already exists.\n')
        except EntityType.DoesNotExist:
            new_entity = EntityType(name=entity.get('name'), weak=entity.get('weak'), eer_diagram=eer_diagram)
            new_entity.save()
    for relationship in relationships:
        source_entity = EntityType.objects.filter(name=relationship.get('initial_entity'), eer_diagram=eer_diagram)[0]
        target_entity = EntityType.objects.filter(name=relationship.get('final_entity'), eer_diagram=eer_diagram)[0]
        cardinality = Cardinality.objects.filter(name=relationship.get('cardinality'))[0]
        participation = Participation.objects.filter(name=relationship.get('participation'))[0]
        try:
            RelationshipType.objects.get(name=relationship.get('name'), eer_diagram=eer_diagram)
            messages.append('Relationship name '+relationship.get('name')+' already exists.\n')
        except RelationshipType.DoesNotExist:
            new_relationship = RelationshipType(name=relationship.get('name'), initial_entity=source_entity, final_entity=target_entity, 
                cardinality=cardinality, participation=participation, eer_diagram=eer_diagram, is_identifier=relationship.get('is_identifier'))
            new_relationship.save()
    for associative_entity in associative_entities:
        relationship = RelationshipType.objects.filter(name=associative_entity.get('relationship'), eer_diagram=eer_diagram)[0]
        try:
            AssociativeEntity.objects.get(name=associative_entity.get('name'), eer_diagram=eer_diagram)
            messages.append('Associative entity name '+associative_entity.get('name')+' already exists.\n')
        except AssociativeEntity.DoesNotExist:
            new_associative_entity = AssociativeEntity(name=associative_entity.get('name'), relationship=relationship, eer_diagram=eer_diagram)
            new_associative_entity.save()
    for attribute in attributes:
        if attribute.get('entity_target'):
            entity_target = EntityType.objects.filter(name=attribute.get('entity_target'), eer_diagram=eer_diagram)[0]
            new_attribute = EntityAttribute(name=attribute.get('name'), attribute_type=attribute.get('attribute_type'), entity_type=entity_target, data_type=attribute.get('data_type'), is_null=attribute.get('is_null'), is_unique=attribute.get('is_unique'), size=attribute.get('size'))
            new_attribute.save()
        if attribute.get('relationship_target'):
            relationship_target = RelationshipType.objects.filter(name=attribute.get('relationship_target'), eer_diagram=eer_diagram)[0]
            new_attribute = RelationshipAttribute(name=attribute.get('name'), attribute_type=attribute.get('attribute_type'), relationship_type=relationship_target, data_type=attribute.get('data_type'), is_null=attribute.get('is_null'), is_unique=attribute.get('is_unique'), size=attribute.get('size'))
            new_attribute.save()
        if attribute.get('attribute_target'):
            attribute_target = EntityAttribute.objects.filter(name=attribute.get('attribute_target'))[0]
            new_attribute = CompositeAttribute(name=attribute.get('name'), attribute_type=attribute.get('attribute_type'), entity_attribute=attribute_target, data_type=attribute.get('data_type'), is_null=attribute.get('is_null'), is_unique=attribute.get('is_unique'), size=attribute.get('size'))
            new_attribute.save()
    for specialization in specializations:
        superclass_entity = EntityType.objects.filter(name=specialization.get('superclass'), eer_diagram=eer_diagram)[0]
        participation = Participation.objects.filter(name=specialization.get('participation'))[0]
        new_specialization = Specialization(name=specialization.get('name'), superclass=superclass_entity, disjoint=specialization.get('disjoint'), participation=participation, eer_diagram=eer_diagram)
        new_specialization.save()
    for subclass in subclasses:
        subclass_entity = EntityType.objects.filter(name=subclass.get('entity'), eer_diagram=eer_diagram)[0]
        subclass_spec = Specialization.objects.filter(name=subclass.get('specialization'), eer_diagram=eer_diagram)[0]
        new_subclass = Subclass(entity_type=subclass_entity, specialization=subclass_spec)
        new_subclass.save()
    for category in categories:
        category_entity = EntityType.objects.filter(name=category.get('category'), eer_diagram=eer_diagram)[0]
        new_category = Category(name=category.get('name'), category=category_entity, eer_diagram=eer_diagram)
        new_category.save()
    for superclass in superclasses:
        superclass_entity = EntityType.objects.filter(name=superclass.get('entity'), eer_diagram=eer_diagram)[0]
        category = Category.objects.filter(name=superclass.get('category'), eer_diagram=eer_diagram)[0]
        participation = Participation.objects.filter(name=superclass.get('participation'))[0]
        new_superclass = Superclass(entity_type=superclass_entity, category=category, participation=participation)
        new_superclass.save()
    for direct_inheritance in direct_inheritances:
        source_entity = EntityType.objects.filter(name=direct_inheritance.get('source'), eer_diagram=eer_diagram)[0]
        target_entity = EntityType.objects.filter(name=direct_inheritance.get('target'), eer_diagram=eer_diagram)[0]
        new_direct_inheritance = DirectInheritance(source=source_entity, target=target_entity, eer_diagram=eer_diagram)
        new_direct_inheritance.save()

    return messages

@login_required
def create_annotation(request, id, atype, action, template_name='annotation.html'):
    if request.POST:
        if 'add_attribute' in request.POST:
            entity_or_relationship = request.POST['entity_relationship_add']
            attribute_list = request.POST['add-attributes']
            new_annotation = 'ALTER '+entity_or_relationship+' ADD ATTRIBUTES '+attribute_list+';'
            if atype == 'relation':
                rel = Relation.objects.filter(pk=id)[0]
                relation_annotation = RelationAnnotation(text=new_annotation, relation=rel)
                relation_annotation.save()
            else:
                cons = Constraint.objects.filter(pk=id)[0]
                constraint_annotation = ConstraintAnnotation(text=new_annotation, constraint=cons)
                constraint_annotation.save()
        else:
            if 'drop_attribute' in request.POST:
                entity_or_relationship = request.POST['entity_relationship_drop']
                attribute_list = request.POST['drop-attributes']
                new_annotation = 'ALTER '+entity_or_relationship+' DROP ATTRIBUTE '+attribute_list+';'
                if atype == 'relation':
                    rel = Relation.objects.filter(pk=id)[0]
                    relation_annotation = RelationAnnotation(text=new_annotation, relation=rel)
                    relation_annotation.save()
                else:
                    cons = Constraint.objects.filter(pk=id)[0]
                    constraint_annotation = ConstraintAnnotation(text=new_annotation, constraint=cons)
                    constraint_annotation.save()
            else:
                if 'add_relationship' in request.POST:
                    relationship_name = request.POST['rel_name']
                    initial_entity = request.POST['initial_entity_relationship']
                    final_entity = request.POST['final_entity_relationship']
                    cardinality = ''
                    if request.POST['rel_cardinality']: 
                        cardinality = request.POST['rel_cardinality']
                    participation = ''
                    if request.POST['rel_participation']:
                        participation = request.POST['rel_participation']
                    attribute_list = ''
                    if request.POST['rel_attributes']:
                        attribute_list = request.POST['rel_attributes']
                    new_annotation = 'ADD RELATIONSHIP '+relationship_name+' BETWEEN '+initial_entity+' AND '+final_entity+' CARDINALITY= '+cardinality+' PARTICIPATION= '+participation+' ATTR= '+attribute_list+';'
                    if atype == 'relation':
                        rel = Relation.objects.filter(pk=id)[0]
                        relation_annotation = RelationAnnotation(text=new_annotation, relation=rel)
                        relation_annotation.save()
                    else:
                        cons = Constraint.objects.filter(pk=id)[0]
                        constraint_annotation = ConstraintAnnotation(text=new_annotation, constraint=cons)
                        constraint_annotation.save()
                else:
                    if 'add_specialization' in request.POST:
                        specialization_name = request.POST['spec_name']
                        superclass = request.POST['superclass']
                        subclass = request.POST['subclass']
                        disjointness = ''
                        if request.POST['spec_disjointness']: 
                            disjointness = request.POST['spec_disjointness']
                        new_annotation = 'ADD SPECIALIZATION '+specialization_name+' FROM SUPERCLASS '+superclass+' TO SUBCLASS '+subclass+' DISJOINTNESS '+disjointness+';'
                        if atype == 'relation':
                            rel = Relation.objects.filter(pk=id)[0]
                            relation_annotation = RelationAnnotation(text=new_annotation, relation=rel)
                            relation_annotation.save()
                        else:
                            cons = Constraint.objects.filter(pk=id)[0]
                            constraint_annotation = ConstraintAnnotation(text=new_annotation, constraint=cons)
                            constraint_annotation.save()
                    else:
                        if 'add_category' in request.POST:
                            category_name = request.POST['cat_name']
                            category = request.POST['category']
                            superclass = request.POST['superclasses']
                            new_annotation = 'ADD CATEGORY '+category_name+' TO CATEGORY '+category+' FROM SUPERCLASS '+superclass+';'
                            if atype == 'relation':
                                rel = Relation.objects.filter(pk=id)[0]
                                relation_annotation = RelationAnnotation(text=new_annotation, relation=rel)
                                relation_annotation.save()
                            else:
                                cons = Constraint.objects.filter(pk=id)[0]
                                constraint_annotation = ConstraintAnnotation(text=new_annotation, constraint=cons)
                                constraint_annotation.save()

    related_relation = ''
    related_constraint = ''
    annotations = ''
    initial_module = ''
    final_module = ''
    label = ''
    if atype == 'relation':
        related_relation = id
        rel = Relation.objects.get(pk=id)
        initial_module = rel.initial_module
        final_module = rel.final_module
        label = rel.label
        annotations = RelationAnnotation.objects.filter(relation=rel)
        dbfd = rel.dbfd
    if atype == 'constraint':
        related_constraint = id
        cons = Constraint.objects.get(pk=id)
        initial_module = cons.initial_module
        final_module = cons.final_module
        label = cons.label
        annotations = ConstraintAnnotation.objects.filter(constraint=cons)
        dbfd = cons.dbfd

    modules = Module.objects.filter(dbfd=dbfd)
    module_entities = []
    module_relationships = []
    attributes = []
    for module in modules:
        if initial_module == module or final_module == module:
            module_eer_diagram = EerDiagram.objects.filter(module_id=module)
            entities = EntityType.objects.filter(eer_diagram=module_eer_diagram)
            for entity in entities:
                module_entities.append({
                    "module_name": module.name,
                    "entity_name": entity.name,
                    "entity_id": entity.pk
                    })
                entity_attributes = EntityAttribute.objects.filter(entity_type=entity)
                for attribute in entity_attributes:
                    attributes.append({
                        "entity": entity.name+'-attributes',
                        "attribute": attribute.name,
                        })
                    composite_attributes = CompositeAttribute.objects.filter(entity_attribute=attribute)
                    for composite in composite_attributes:
                        attributes.append({
                            "entity": entity.name+'-attributes',
                            "attribute": composite.name,
                        })
            relationships = RelationshipType.objects.filter(eer_diagram=module_eer_diagram)
            for relationship in relationships:
                module_relationships.append({
                    "module_name": module.name,
                    "relationship_name": relationship.name
                    })
                relationship_attributes = RelationshipAttribute.objects.filter(relationship_type=relationship)
                for attribute in relationship_attributes:
                    attributes.append({
                        "entity": relationship.name+'-attributes',
                        "attribute": attribute.name,
                        })
    cardinalities = Cardinality.objects.all()
    participations = Participation.objects.all()
    configuration = Configuration.objects.filter(dbfd=dbfd)

    context = {"action":action, "module_entities": module_entities, "module_relationships": module_relationships, 
    "cardinalities": cardinalities, "participations": participations, "relation": related_relation, 
    "constraint": related_constraint, "annotations": annotations, "attributes": attributes, "label": label,
    "config": configuration, "dbfd": dbfd}
    return render(request, template_name, context)

@login_required
def delete_annotation(request, id, atype):
    if atype == 'relation':
        annotation = RelationAnnotation.objects.filter(pk=id)[0]
        relation = Relation.objects.filter(pk=annotation.relation.pk)[0]
        annotation.delete()
        return redirect('annotation', id=relation.pk, atype=atype, action="plus")
    else:
        annotation = ConstraintAnnotation.objects.filter(pk=id)[0]
        constraint = Constraint.objects.filter(pk=annotation.constraint.pk)[0]
        annotation.delete()
        return redirect('annotation', id=constraint.pk, atype=atype, action="plus")

@login_required
def create_configuration(request, id, template_name='configuration.html'):
    module_list = []
    dbfd = DBFD.objects.get(pk=id)
    modules = Module.objects.filter(dbfd=dbfd)
    hierarchy = []
    node_id = 0
    for module in modules:
        id_attr = module.name
        button_type = 'checkbox'
        name_attr = module.name
        module_type = ''
        if module.parent is None:
            module_type = 'mandatory'
        else:
            parental_relation = Relation.objects.filter(initial_module=module.parent, final_module=module)
            if parental_relation[0].relation_type.rtype == 'and':
                if module.mandatory:
                    module_type = 'mandatory'
                else:
                    module_type = 'optional'
            else:
                if parental_relation[0].relation_type.rtype == 'or':
                    module_type = 'or'
                else:
                    module_type = 'alternative'
        if module.parent is None:
            module_list.append({ "module": module, "level": node_id, "parent_level": 0, "padding": 0, "id": id_attr,
            "checked": 'checked', "disabled": 'disabled', "elemtype": button_type, "name": name_attr, "type": module_type})
            hierarchy.append({ "name": module.name, "level": node_id, "child": 1, "padding": 0, "first_child": True})
        else:
            parent_relation = Relation.objects.filter(initial_module=module.parent, final_module=module)
            checked = ''
            disabled = ''
            for h in hierarchy:
                parent_name = module.parent.name
                if h["name"] == parent_name:
                    parent_id = h["level"]
                    parent_padding = h["padding"]
                    child_level = h["child"]
                    first_child = h["first_child"]
            if module.mandatory:
                checked = 'checked'
                disabled = 'disabled'
            else:
                if parent_relation[0].relation_type.rtype == 'or' or parent_relation[0].relation_type.rtype == 'alternative':
                    if first_child:
                        checked = 'checked'
                        for h in hierarchy:
                            parent_name = module.parent.name
                            if h["name"] == parent_name:
                                h["first_child"] = False 
                if parent_relation[0].relation_type.rtype == 'alternative':
                    button_type = 'radio'
                    name_attr = 'xor-'+module.parent.name
                if parent_relation[0].relation_type.rtype == 'or':
                    name_attr = 'or-'+module.parent.name
            module_list.insert(child_level, {"module": module, "level": child_level, "parent_level": parent_id, 
                "padding": 19*(parent_padding+1), "id": id_attr, "checked": checked, "disabled": disabled,
                "elemtype": button_type, "name": name_attr, "type": module_type})
            hierarchy.append({ "name": module.name, "level": child_level, "child": child_level + 1, 
                "padding": parent_padding+1, "first_child": True})
            parental_list = []
            parental_list.append(module.parent)
            for parent in parental_list:
                for element in hierarchy:
                    if element['name'] == parent.name:
                        element['child'] = element['child'] + 1
                grand_parent = parent.parent
                if grand_parent:
                    parental_list.append(grand_parent)
            if child_level < node_id:
                for element in hierarchy:
                    if element['level'] >= child_level and element['name'] != module.name:
                        element['level'] = element['level'] + 1
                        element['child'] = element['child'] + 1
                for element in module_list:
                    element['level'] = module_list.index(element)                   
                    if element['parent_level'] >= child_level and element['module'] != module.name:
                        element['parent_level'] = element['parent_level'] + 1
        node_id += 1        

    constraint_list = []
    constraints = Constraint.objects.filter(dbfd=dbfd)
    for constraint in constraints:
        constraint_list.append({
            "initial": constraint.initial_module.name,
            "type": constraint.constraint_type.ctype,
            "final": constraint.final_module.name
            })

    checked = ''
    error_messages = ''
    valid = True
    eer_diagram = ''
    # document = ''
    file_name = ''
    added_modules = []
    removed_modules = []

    if request.POST:
        checked = request.POST.getlist('option')
        for module in modules:
            if module.mandatory and module.parent:
                checked.append(module.name)
            if not module.parent:
                root = module.name
        for element in module_list:
            if element['id'] in checked or element['parent_level'] == '':
                element['checked'] = 'checked'
            else:
                element['checked'] = ''
        for check in checked:
            module_check = Module.objects.filter(name=check)[0]
            if module_check.parent.name not in checked and module_check.parent.name != root and not module_check.parent.abstract:
                error_messages = error_messages + "\n You can not select "+check+" module without checking his parent."
                valid = False
            constraints = Constraint.objects.filter(initial_module=module_check)
            for constraint in constraints:
                if constraint.constraint_type.ctype == 'requires':
                    if constraint.final_module.name not in checked:
                        error_messages = error_messages + "\n"+constraint.initial_module.name+" module requires "+constraint.final_module.name+" module also."
                        valid = False
                if constraint.constraint_type.ctype == 'excludes':
                    if constraint.final_module.name in checked:
                        error_messages = error_messages + "\nYou can not have "+constraint.initial_module.name+" and "+constraint.final_module.name+ " modules at the same configuration!"
                        valid = False
        if 'eer' in request.POST and valid:
            eer_diagram = generate_eer_diagram(checked, root, dbfd)
        else:
            if 'sql-new' in request.POST and valid:
                sql_script = generate_new_sql_script(checked, dbfd)
                sql_script = sql_script.replace(" ","_").replace(":", "")
                file_name = 'documents/'+sql_script
            else:
                if 'sql-update' in request.POST and valid:
                    sql_script = generate_update_sql_script(checked, dbfd)
                    sql_script = sql_script.replace(" ","_").replace(":", "")
                    file_name = 'documents/'+sql_script

    context = {"list": module_list, "constraints": constraint_list, "checked": checked, "error_messages": error_messages, 
        "valid": valid, "eer_diagram": eer_diagram, 'sql_script': file_name, "added": added_modules, 
        "removed": removed_modules, "dbfd_id": id}
    return render_to_response(template_name, context, context_instance=RequestContext(request))

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        result = s[start:end]
        return result.strip(' ')
    except ValueError:
        return ""

def generate_eer_diagram(checked, root, dbfd):
    diagrams = []
    annotation_list = []
    root_module = Module.objects.filter(name=root, dbfd=dbfd)
    root_relations = Relation.objects.filter(initial_module=root_module)
    for relation in root_relations:
        if relation.final_module.name in checked:
            annotations = RelationAnnotation.objects.filter(relation=relation)
            for annotation in annotations:
                annotation_list.append({'text': annotation.text})                    
    for check in checked:
        module = Module.objects.filter(name=check, dbfd=dbfd)
        eer_diagram = EerDiagram.objects.get(module_id=module)
        diagrams.append({'eer': eer_diagram})
        relations = Relation.objects.filter(initial_module=module)
        for relation in relations:
            if relation.final_module.name in checked:
                annotations = RelationAnnotation.objects.filter(relation=relation)
                for annotation in annotations:
                    annotation_list.append({'text': annotation.text})                    
        constraints = Constraint.objects.filter(initial_module=module)
        for constraint in constraints:
            if constraint.final_module.name in checked:
                annotations = ConstraintAnnotation.objects.filter(constraint=constraint)
                for annotation in annotations:
                    annotation_list.append({'text': annotation.text})

    removed_attributes = []
    for annotation in annotation_list:
        if 'DROP ATTRIBUTE' in annotation['text']:
            attribute = find_between(annotation['text'], 'DROP ATTRIBUTE', ';')
            removed_attributes.append(attribute)

    # graph = pydot.Dot(graph_type='digraph', rankdir = 'LR')
    graph = pydot.Dot(graph_type='digraph')
    disjoint_count = 1
    overlap_count = 1
    category_count = 1
    nodes = {}
    for diagram in diagrams:
        entities = EntityType.objects.filter(eer_diagram=diagram['eer'])
        relationships = RelationshipType.objects.filter(eer_diagram=diagram['eer'])
        specializations = Specialization.objects.filter(eer_diagram=diagram['eer'])
        direct_inheritances = DirectInheritance.objects.filter(eer_diagram=diagram['eer'])
        categories = Category.objects.filter(eer_diagram=diagram['eer'])
        for entity in entities:
            varname = entity.name
            if entity.weak:
                nodes[varname] = pydot.Node(entity.name, style="filled, diagonals", shape="box", fillcolor="turquoise4")
            else:
                nodes[varname] = pydot.Node(entity.name, style="filled", shape="box", fillcolor="turquoise4")
            graph.add_node(nodes[varname])
            entity_attributes = EntityAttribute.objects.filter(entity_type=entity)
            for attribute in entity_attributes:
                attr_name = attribute.name
                if attr_name not in removed_attributes:
                    if attribute.attribute_type == 'IDENTIFIER':
                        nodes[attr_name] = pydot.Node(attribute.name, shape="ellipse", style="filled, bold", fillcolor="gainsboro")
                    else:
                        if attribute.attribute_type == 'MULTIVALUED':
                            nodes[attr_name] = pydot.Node(attribute.name, shape="ellipse", style="filled, dashed", fillcolor="gainsboro")
                        else:
                            nodes[attr_name] = pydot.Node(attribute.name, shape="ellipse", style="filled", fillcolor="gainsboro")
                    graph.add_node(nodes[attr_name])
                    graph.add_edge(pydot.Edge(nodes[varname], nodes[attr_name], arrowhead='none'))   
                    composite_attributes = CompositeAttribute.objects.filter(entity_attribute=attribute)
                    for composite_attribute in composite_attributes:
                        composite_attr_name = composite_attribute.name
                        if composite_attr_name not in removed_attributes:
                            if composite_attribute.attribute_type == 'IDENTIFIER':
                                nodes[composite_attr_name] = pydot.Node(composite_attr_name, shape="ellipse", style="filled, bold", fillcolor="gainsboro")
                            else:
                                if composite_attribute.attribute_type == 'MULTIVALUED':
                                    nodes[composite_attr_name] = pydot.Node(composite_attr_name, shape="ellipse", style="filled, dashed", fillcolor="gainsboro")
                                else:
                                    nodes[composite_attr_name] = pydot.Node(composite_attr_name, shape="ellipse", style="filled", fillcolor="gainsboro")
                            graph.add_node(nodes[composite_attr_name])
                            graph.add_edge(pydot.Edge(nodes[attr_name], nodes[composite_attr_name], arrowhead='none')) 
        for relationship in relationships:
            varname = relationship.name
            if relationship.is_identifier:
                nodes[varname] = pydot.Node(relationship.name, style="filled", shape="Mdiamond", fillcolor="cadetblue2")
            else:
                nodes[varname] = pydot.Node(relationship.name, style="filled", shape="diamond", fillcolor="cadetblue2")
            associative_entity = AssociativeEntity.objects.filter(relationship=relationship)
            if associative_entity:
                cluster = pydot.Cluster(associative_entity[0].name,label=associative_entity[0].name)
                graph.add_subgraph(cluster)
                cluster.add_node(nodes[varname])
            else:
                graph.add_node(nodes[varname])
            relationship_attributes = RelationshipAttribute.objects.filter(relationship_type=relationship)
            for attribute in relationship_attributes:
                attr_name = attribute.name
                if attr_name not in removed_attributes:
                    if attribute.attribute_type == 'MULTIVALUED':
                        nodes[attr_name] = pydot.Node(attribute.name, shape="ellipse", style="filled, dashed", fillcolor="gainsboro")
                    else:
                        nodes[attr_name] = pydot.Node(attribute.name, shape="ellipse", style="filled", fillcolor="gainsboro")
                    graph.add_node(nodes[attr_name])
                    graph.add_edge(pydot.Edge(nodes[varname], nodes[attr_name], arrowhead='none'))  
            initial = relationship.initial_entity.name
            final = relationship.final_entity.name
            initial_edge = ""
            final_edge = ""  
            cardinality = relationship.cardinality.name.strip(':')
            if relationship.participation.name == 'TOTAL:TOTAL':
                initial_edge = "bold"
                final_edge = "bold"
            else:
                if relationship.participation.name == 'TOTAL:PARTIAL':
                    initial_edge = "bold"
                    final_edge = ""    
                else:
                    if relationship.participation.name == 'PARTIAL:TOTAL':
                        initial_edge = ""
                        final_edge = "bold"      
            graph.add_edge(pydot.Edge(nodes[initial], nodes[varname], label=cardinality[0], arrowhead='none', style=initial_edge))
            graph.add_edge(pydot.Edge(nodes[varname], nodes[final], label=cardinality[2], arrowhead='none', style=final_edge))
        for specialization in specializations:
            varname = specialization.name
            if not varname in nodes:
                if specialization.disjoint:
                    nodes[varname] = pydot.Node("d"+disjoint_count*'\'', style="filled", shape="circle", fillcolor="cadetblue2")
                    disjoint_count += 1
                else:
                    nodes[varname] = pydot.Node("o"+overlap_count*'\'', style="filled", shape="circle", fillcolor="cadetblue2")
                    overlap_count += 1
                graph.add_node(nodes[varname])
            superclass = specialization.superclass.name
            if specialization.participation.name == 'TOTAL':
                graph.add_edge(pydot.Edge(nodes[superclass], nodes[varname], arrowhead='none', style='bold'))
            else:
                graph.add_edge(pydot.Edge(nodes[superclass], nodes[varname], arrowhead='none'))
            subclasses = Subclass.objects.filter(specialization=specialization)
            for subclass in subclasses:
                graph.add_edge(pydot.Edge(nodes[varname], nodes[subclass.entity_type.name], arrowhead='curve'))
        for category in categories:
            varname = category.name
            if not varname in nodes:
                nodes[varname] = pydot.Node("u"+category_count*'\'', style="filled", fillcolor="cadetblue2", shape="circle")
                graph.add_node(nodes[varname])
                category_count += 1
            superclasses = Superclass.objects.filter(category=category)
            for superclass in superclasses:
                if superclass.participation.name == 'TOTAL':
                    graph.add_edge(pydot.Edge(nodes[superclass.entity_type.name], nodes[varname], arrowhead='none', style='bold'))
                else:
                    graph.add_edge(pydot.Edge(nodes[superclass.entity_type.name], nodes[varname], arrowhead='none'))
            category_class = category.category.name
            graph.add_edge(pydot.Edge(nodes[varname], nodes[category_class], arrowhead='curve')) 
        for direct_inheritance in direct_inheritances:
            source = direct_inheritance.source.name
            target = direct_inheritance.target.name
            graph.add_edge(pydot.Edge(nodes[source], nodes[target], arrowhead='curve'))

    for annotation in annotation_list:
        if annotation['text'].startswith('ADD RELATIONSHIP'):
            rel_name = find_between(annotation['text'], 'ADD RELATIONSHIP', 'BETWEEN')
            nodes[rel_name] = pydot.Node(rel_name, style="filled", shape="diamond", fillcolor="cadetblue2")
            graph.add_node(nodes[rel_name])
            initial_module = find_between(annotation['text'], 'BETWEEN', 'AND')
            final_module = find_between(annotation['text'], 'AND', 'CARDINALITY')
            initial_entity = find_between(initial_module, '- E ', ')')
            final_entity = find_between(final_module, '- E ', ')')
            cardinality = find_between(annotation['text'], 'CARDINALITY=', 'PARTICIPATION')
            cardinality = cardinality.strip(':')
            participation = find_between(annotation['text'], 'PARTICIPATION=', 'ATTR=')
            initial_edge = ""
            final_edge = ""
            if participation:
                if participation == 'TOTAL:TOTAL':
                    initial_edge = "bold"
                    final_edge = "bold"
                else:
                    if participation == 'TOTAL:PARTIAL':
                        initial_edge = "bold"
                        final_edge = ""    
                    else:
                        if participation == 'PARTIAL:TOTAL':
                            initial_edge = ""
                            final_edge = "bold" 
            graph.add_edge(pydot.Edge(nodes[initial_entity], nodes[rel_name], label=cardinality[0], arrowhead='none', style=initial_edge))
            graph.add_edge(pydot.Edge(nodes[rel_name], nodes[final_entity], label=cardinality[2], arrowhead='none', style=final_edge))
            attributes = find_between(annotation['text'], 'ATTR=', ';')
            if attributes:
                attributes = attributes.split('-')
                for attribute in attributes:
                    if '{' in attribute:
                        attr = find_between(attribute, '{', '}')
                        nodes[attr] = pydot.Node(attr, shape="ellipse", style="filled, dashed", fillcolor="gainsboro")
                        graph.add_node(nodes[attr])
                        graph.add_edge(pydot.Edge(nodes[rel_name], nodes[attr], arrowhead='none'))      
                    else:
                        nodes[attribute] = pydot.Node(attribute, shape="ellipse", style="filled", fillcolor="gainsboro")
                        graph.add_node(nodes[attribute])
                        graph.add_edge(pydot.Edge(nodes[rel_name], nodes[attribute], arrowhead='none'))  
        else:
            if 'ADD ATTRIBUTES' in annotation['text']:
                table = find_between(annotation['text'], '- E ', ')')
                if not table:
                    table = find_between(annotation['text'], '- R ', ')')
                attributes = find_between(annotation['text'], 'ADD ATTRIBUTES ', ';')
                attributes = attributes.split('-')
                composite = False
                composite_parent = []
                for attribute in attributes:
                    multivalued = False
                    attribute = attribute.strip(' ')
                    if attribute.startswith('{'):
                        multivalued = True
                        attribute = find_between(attribute, '{', '}')
                    attrs = attribute.split(',')
                    for attr in attrs:
                        if '(' in attr:
                            attr = attr.split('(')
                            composite_attr = attr[0]
                            simple = attr[1].replace(')', '')
                            style = 'filled'
                            if multivalued:
                                style += ', dashed'
                                multivalued = False
                            if composite_attr.startswith('KEY'):
                                composite_attr = composite_attr.replace('KEY', '')
                                style += ', bold'
                            nodes[composite_attr] = pydot.Node(composite_attr, shape="ellipse", style=style, fillcolor="gainsboro")
                            graph.add_node(nodes[composite_attr]) 
                            nodes[simple] = pydot.Node(simple, shape="ellipse", style="filled", fillcolor="gainsboro")       
                            graph.add_node(nodes[simple])
                            graph.add_edge(pydot.Edge(nodes[composite_attr], nodes[simple], arrowhead='none'))
                            if composite_parent:
                                parent = composite_parent[len(composite_parent)-1]
                                graph.add_edge(pydot.Edge(nodes[parent], nodes[composite_attr], arrowhead='none'))
                            else:
                                graph.add_edge(pydot.Edge(nodes[table], nodes[composite_attr], arrowhead='none'))  
                            if ')' not in attr:
                                composite_parent.append(composite_attr)
                        else:
                            if ')' in attr:
                                attr = attr.replace(')', '')
                                nodes[attr] = pydot.Node(attr, shape="ellipse", style="filled", fillcolor="gainsboro")       
                                graph.add_node(nodes[attr])
                                parent = composite_parent[len(composite_parent)-1]
                                graph.add_edge(pydot.Edge(nodes[parent], nodes[attr], arrowhead='none'))
                                composite_parent.pop()
                            else:
                                style = 'filled'
                                if multivalued:
                                    style += ', dashed'
                                if attr.startswith('KEY'):
                                    attr = attr.replace('KEY', '')
                                    style += ', bold'
                                nodes[attr] = pydot.Node(attr, shape="ellipse", style=style, fillcolor="gainsboro")
                                graph.add_node(nodes[attr]) 
                                graph.add_edge(pydot.Edge(nodes[table], nodes[attr], arrowhead='none'))
            else:
                if annotation['text'].startswith('ADD SPECIALIZATION'):
                    spec_name = find_between(annotation['text'], 'SPECIALIZATION', 'FROM')
                    superclass = find_between(annotation['text'], 'FROM SUPERCLASS', 'TO')
                    superclass_entity = find_between(superclass, '- E ', ')')
                    subclass = find_between(annotation['text'], 'TO SUBCLASS', 'DISJOINTNESS')
                    subclass_entity = find_between(subclass, '- E ', ')')
                    disjoint = find_between(annotation['text'], 'DISJOINTNESS', ';')
                    if not spec_name in nodes:
                        if disjoint == 'disjoint':
                            nodes[spec_name] = pydot.Node("d"+disjoint_count*'\'', style="filled", shape="circle", fillcolor="cadetblue2")
                            disjoint_count += 1
                        else:
                            nodes[spec_name] = pydot.Node("o"+overlap_count*'\'', style="filled", shape="circle", fillcolor="cadetblue2")
                            overlap_count += 1
                    graph.add_node(nodes[spec_name])
                    graph.add_edge(pydot.Edge(nodes[superclass_entity], nodes[spec_name], arrowhead='none'))
                    graph.add_edge(pydot.Edge(nodes[spec_name], nodes[subclass_entity], arrowhead='curve'))
                else:
                    if annotation['text'].startswith('ADD CATEGORY'):
                        cat_name = find_between(annotation['text'], 'ADD CATEGORY', 'TO')
                        category = find_between(annotation['text'], 'TO CATEGORY', 'FROM')
                        category_entity = find_between(category, '- E ', ')')
                        superclass = find_between(annotation['text'], 'FROM SUPERCLASS', ';')
                        superclass_entity = find_between(superclass, '- E ', ')')
                        if not cat_name in nodes:
                            nodes[cat_name] = pydot.Node("u"+category_count*'\'', style="filled", shape="circle", fillcolor="cadetblue2")
                            graph.add_node(nodes[cat_name])
                            category_count += 1
                        graph.add_edge(pydot.Edge(nodes[cat_name], nodes[category_entity], arrowhead='curve'))
                        graph.add_edge(pydot.Edge(nodes[superclass_entity], nodes[cat_name], arrowhead='none'))

    file_name = 'diagram-'+time.strftime("%d-%m-%Y %H:%M:%S")+'.png'
    graph.write_png('dbfd/static/dbfd/images/'+file_name)
    return file_name

def generate_create_table_script(diagrams):
    sql_script = []
    tables = []

    for diagram in diagrams:
        entities = EntityType.objects.filter(eer_diagram=diagram['eer'])
        relationships = RelationshipType.objects.filter(eer_diagram=diagram['eer'])
        specializations = Specialization.objects.filter(eer_diagram=diagram['eer'])
        direct_inheritances = DirectInheritance.objects.filter(eer_diagram=diagram['eer'])
        categories = Category.objects.filter(eer_diagram=diagram['eer'])
        
        for entity in entities:
            primary_key = entity.name+'Id'
            foreign_key = ''
            if entity.weak:
                weak_relationships = RelationshipType.objects.filter(is_identifier=True)
                for weak_relationship in weak_relationships:
                    if weak_relationship.initial_entity == entity:
                        for table in tables:
                            if table['name'] == weak_relationship.final_entity.name:
                                primary_key = table['key']
                                foreign_key = table['key']
                                father = table['name']
                        break
                    else:
                        if weak_relationship.final_entity == entity:
                            for table in tables:
                                if table['name'] == weak_relationship.initial_entity.name:
                                    primary_key = table['key']
                                    foreign_key = table['key']
                                    father = table['name']
                        break
            script = 'CREATE TABLE '+entity.name+' (\n'
            entity_attributes = EntityAttribute.objects.filter(entity_type=entity)
            if entity.weak:
                script += primary_key+' int,\n'   
            for entity_attribute in entity_attributes:
                if entity_attribute.attribute_type == 'COMMON':
                    composite_attributes = CompositeAttribute.objects.filter(entity_attribute=entity_attribute)
                    if composite_attributes:
                        for composite_attribute in composite_attributes:
                            script += composite_attribute.name+' '+composite_attribute.data_type+'('+str(composite_attribute.size)+'),\n' 
                    else:       
                        script += entity_attribute.name+' '+entity_attribute.data_type+'('+str(entity_attribute.size)+'),\n'
                if entity_attribute.attribute_type == 'IDENTIFIER':
                    script += entity_attribute.name+' '+entity_attribute.data_type+'('+str(entity_attribute.size)+') NOT NULL,\n'   
                    if entity.weak:
                        primary_key += ', '+entity_attribute.name
                    else:
                        primary_key = entity_attribute.name
                if entity_attribute.attribute_type == 'MULTIVALUED':
                    multivalued_script = 'create table '+entity.name+entity_attribute.name+' (\n'
                    multivalued_script += entity_attribute.name+' '+entity_attribute.data_type+'('+str(entity_attribute.size)+'),\n'
                    multivalued_script += 'Id int primary key,\n'
                    multivalued_script += entity.name+ 'Id int NOT NULL REFERENCES '+entity.name+'('+primary_key+')\n)\n'
                    sql_script.append({
                        'table': entity.name+entity_attribute.name,
                        'script': multivalued_script
                        })
            if foreign_key:
                script += 'FOREIGN KEY ('+foreign_key+') REFERENCES '+father+'('+foreign_key+'),\n'
            if primary_key != entity.name+'Id':
                script += 'PRIMARY KEY ('+primary_key+')\n)\n'
            else:
                script += primary_key+' int NOT NULL,\nPRIMARY KEY ('+entity.name+'Id)\n)\n'
            sql_script.append({
                'table': entity.name,
                'script': script
                })
            tables.append({
                'name': entity.name,
                'key': primary_key
                })
        
        for relationship in relationships:
            # if relationship.is_identifier:  
            relationship_attributes = RelationshipAttribute.objects.filter(relationship_type=relationship)
            associative_entity = AssociativeEntity.objects.filter(relationship=relationship)
            if relationship.cardinality.name == 'M:N' or associative_entity:
                script = 'CREATE TABLE '+relationship.name+' (\n'
                for attribute in relationship_attributes:
                    script += attribute.name+' '+attribute.data_type+'('+str(attribute.size)+'),\n'
                for table in tables:
                    if table['name'] == relationship.initial_entity.name:
                        first_key = table['key']
                    else:
                        if table['name'] == relationship.final_entity.name:
                            second_key = table['key']
                participation = relationship.participation.name
                if participation.startswith('PARTIAL'):
                    script += first_key +' int,\n'
                else:
                    script += first_key +' int NOT NULL,\n'
                if participation.endswith(':PARTIAL'):
                    script += second_key + ' int,\n'
                else:
                    script += second_key + ' int NOT NULL,\n'
                script += 'PRIMARY KEY ('+ first_key+', '+second_key+'),\n'
                if participation.startswith('PARTIAL'):
                    script += 'FOREIGN KEY ('+first_key+') REFERENCES '+relationship.initial_entity.name+' ON DELETE SET NULL, \n'
                else:
                    script += 'FOREIGN KEY ('+first_key+') REFERENCES '+relationship.initial_entity.name+' ON DELETE NO ACTION, \n'
                if participation.endswith(':PARTIAL'):
                    script += 'FOREIGN KEY ('+second_key+') REFERENCES '+relationship.final_entity.name+' ON DELETE SET NULL )\n'
                else:
                    script += 'FOREIGN KEY ('+second_key+') REFERENCES '+relationship.final_entity.name+' ON DELETE NO ACTION )\n'
                sql_script.append({
                    'table': relationship.name,
                    'script': script
                })
                tables.append({
                    'name': relationship.name,
                    'key': first_key+','+second_key
                })
            else:
                if relationship.cardinality.name == '1:1':
                    if relationship.participation.name == 'PARTIAL:TOTAL':
                        choose_entity = relationship.final_entity.name
                        other_entity = relationship.initial_entity.name
                        foreign_key_declare = 'ON DELETE NO ACTION'
                    else:
                        choose_entity = relationship.initial_entity.name
                        other_entity = relationship.final_entity.name
                        foreign_key_declare = 'ON DELETE NO ACTION'
                else:
                    if relationship.cardinality.name == '1:N':
                        choose_entity = relationship.initial_entity.name
                        other_entity = relationship.final_entity.name
                        if relationship.participation.name.startswith('PARTIAL'):
                            foreign_key_declare = 'ON DELETE SET NULL'
                        else:
                            foreign_key_declare = 'ON DELETE NO ACTION'
                    else:
                        if relationship.cardinality.name == 'M:1':
                            choose_entity = relationship.final_entity.name
                            other_entity = relationship.initial_entity.name
                            if relationship.participation.name.endswith(':PARTIAL'):
                                foreign_key_declare = 'ON DELETE SET NULL'
                            else:
                                foreign_key_declare = 'ON DELETE NO ACTION'
                for table in tables:
                    if table['name'] == other_entity:
                        primary_key = table['key']
                        break
                for script in sql_script:
                    new_script = ''
                    if script['table'] == choose_entity:
                        old_script = script['script']
                        if primary_key not in old_script:
                            old_script = old_script[:-3]+',\n'
                            for attribute in relationship_attributes:
                                new_script += attribute.name+' '+attribute.data_type+'('+str(attribute.size)+'),\n'
                            if foreign_key_declare == 'ON DELETE NO ACTION':
                                new_script += primary_key+' int NOT NULL,\n'
                            else:
                                new_script += primary_key+' int,\n'
                            new_script += 'FOREIGN KEY ('+primary_key+') REFERENCES '+other_entity+'('+primary_key+') '+foreign_key_declare+'\n)\n'
                            script['script'] = old_script + new_script
                        break

        for specialization in specializations:
            for table in tables:
                if table['name'] == specialization.superclass.name:
                    primary_key = table['key']
                    break
            subclasses = Subclass.objects.filter(specialization=specialization)
            for subclass in subclasses:
                for script in sql_script:
                    new_script = ''
                    if script['table'] == subclass.entity_type.name:
                        old_script = script['script']
                        if primary_key not in old_script:
                            old_script = old_script[:-3]+',\n'
                            new_script += primary_key+' int,\n'
                            new_script += 'FOREIGN KEY ('+primary_key+') REFERENCES '+specialization.superclass.name+'('+primary_key+')\n)\n'
                            script['script'] = old_script + new_script

        for category in categories:
            for table in tables:
                if table['name'] == category.category.name:
                    primary_key = table['key']
                    break
            superclasses = Superclass.objects.filter(category=category)
            for superclass in superclasses:
                for script in sql_script:
                    new_script = ''
                    if script['table'] == superclass.entity_type.name:
                        old_script = script['script']
                        if primary_key not in old_script:
                            old_script = old_script[:-3]+',\n'
                            new_script += primary_key+' int,\n'
                            new_script += 'FOREIGN KEY ('+primary_key+') REFERENCES '+category.category.name+'('+primary_key+')\n)\n'
                            script['script'] = old_script + new_script

        for direct_inheritance in direct_inheritances:
            source = direct_inheritance.source.name
            target = direct_inheritance.target.name
            target_script = ''
            for script in sql_script:
                if script['table'] == target:
                    target_script = script['script']
                    sql_script.remove(script)
                if script['table'] == source:
                    old_script = script['script']
                    old_script = old_script[:-3]+',\n'
                    target_script = target_script[:-3]
                    target_script = target_script.replace('CREATE TABLE '+target+' (\n', '')
                    target_script += target+'Flag int \n)\n'
                    script['script'] = old_script + target_script

    return sql_script, tables

def generate_annotations_script(annotation_list, sql_script, tables):
    for annotation in annotation_list:
        if annotation['text'].startswith('ADD RELATIONSHIP'):
            rel_name = find_between(annotation['text'], 'ADD RELATIONSHIP', 'BETWEEN')
            initial_module = find_between(annotation['text'], 'BETWEEN', 'AND')
            final_module = find_between(annotation['text'], 'AND', 'CARDINALITY')
            initial_entity = find_between(initial_module, '- E ', ')')
            final_entity = find_between(final_module, '- E ', ')')
            cardinality = find_between(annotation['text'], 'CARDINALITY=', 'PARTICIPATION')
            participation = find_between(annotation['text'], 'PARTICIPATION=', 'ATTR=')
            attributes = find_between(annotation['text'], 'ATTR=', ';')
            if attributes:
                attributes = attributes.split('-')
            if cardinality == 'M:N':
                script = 'CREATE TABLE '+rel_name+' (\n'
                for attribute in attributes:
                    script += attribute+' STRING(20),\n'
                for table in tables:
                    if table['name'] == initial_entity:
                        first_key = table['key']
                    else:
                        if table['name'] == final_entity:
                            second_key = table['key']
                if participation.startswith('PARTIAL'):
                    script += first_key +' int,\n'
                else:
                    script += first_key +' int NOT NULL,\n'
                if participation.endswith(':PARTIAL'):
                    script += second_key + ' int,\n'
                else:
                    script += second_key + ' int NOT NULL,\n'
                script += 'PRIMARY KEY ('+ first_key+', '+second_key+'),\n'
                if participation.startswith('PARTIAL'):
                    script += 'FOREIGN KEY ('+first_key+') REFERENCES '+initial_entity+' ON DELETE SET NULL, \n'
                else:
                    script += 'FOREIGN KEY ('+first_key+') REFERENCES '+initial_entity+' ON DELETE NO ACTION, \n'
                if participation.endswith(':PARTIAL'):
                    script += 'FOREIGN KEY ('+second_key+') REFERENCES '+final_entity+' ON DELETE SET NULL )\n'
                else:
                    script += 'FOREIGN KEY ('+second_key+') REFERENCES '+final_entity+' ON DELETE NO ACTION )\n'
                sql_script.append({
                    'table': rel_name,
                    'script': script
                })
                tables.append({
                    'name': rel_name,
                    'key': first_key+','+second_key
                })
            else:
                if cardinality == '1:1':
                    if participation == 'PARTIAL:TOTAL':
                        choose_entity = final_entity
                        other_entity = initial_entity
                        foreign_key_declare = 'ON DELETE NO ACTION'
                    else:
                        choose_entity = initial_entity
                        other_entity = final_entity
                        foreign_key_declare = 'ON DELETE NO ACTION'
                else:
                    if cardinality == '1:N':
                        choose_entity = initial_entity
                        other_entity = final_entity
                        if participation.startswith('PARTIAL'):
                            foreign_key_declare = 'ON DELETE SET NULL'
                        else:
                            foreign_key_declare = 'ON DELETE NO ACTION'
                    else:
                        if cardinality == 'M:1':
                            choose_entity = final_entity
                            other_entity = initial_entity
                            if participation.endswith(':PARTIAL'):
                                foreign_key_declare = 'ON DELETE SET NULL'
                            else:
                                foreign_key_declare = 'ON DELETE NO ACTION'
                for table in tables:
                    if table['name'] == other_entity:
                        primary_key = table['key']
                        break
                for script in sql_script:
                    new_script = ''
                    if script['table'] == choose_entity:
                        old_script = script['script']
                        if primary_key not in old_script:
                            old_script = old_script[:-3]+',\n'
                            for attribute in attributes:
                                new_script += attribute+' STRING(20),\n'
                            if foreign_key_declare == 'ON DELETE NO ACTION':
                                new_script += primary_key+' int NOT NULL,\n'
                            else:
                                new_script += primary_key+' int,\n'
                            new_script += 'FOREIGN KEY ('+primary_key+') REFERENCES '+other_entity+'('+primary_key+') '+foreign_key_declare+'\n)\n'
                            script['script'] = old_script + new_script
                        break
                    
        else:
            if 'ADD ATTRIBUTES' in annotation['text']:
                table_name = find_between(annotation['text'], '- E ', ')')
                if not table_name:
                    table_name = find_between(annotation['text'], '- R ', ')')
                attributes = find_between(annotation['text'], 'ADD ATTRIBUTES ', ';')
                attributes = attributes.split('-')
                for attribute in attributes:
                    composite_attrs = []
                    if attribute.startswith('{'):
                        attribute = find_between(attribute, '{', '}')
                        for table in tables:
                            if table['name'] == table_name:
                                primary_key = table['key']
                                break
                        multivalued_script = 'create table '+table_name+attribute+' (\n'
                        multivalued_script += attribute+' STRING(20),\n'
                        multivalued_script += 'Id int primary key,\n'
                        multivalued_script += table_name+ 'Id int NOT NULL REFERENCES '+table_name+'('+primary_key+')\n)\n'
                        sql_script.append({
                            'table': table_name+attribute,
                            'script': multivalued_script
                            })
                    else:
                        if attribute.startswith('KEY'):
                            attribute_name = find_between(attribute, 'KEY ','')
                            attribute_type = 'key'
                        elif '(' in attribute:
                            composite = find_between(attribute, '(', ')')
                            composite_attrs = composite.split(',')
                        else:
                            attribute_name = attribute.strip()
                            attribute_type = 'simple'
                        for script in sql_script:
                            new_script = ''
                            if script['table'] == table_name:
                                old_script = script['script']
                                old_script = old_script[:-3]+',\n'
                                if attribute_type == 'key':
                                    if attribute_name not in old_script:
                                        new_script += attribute_name+' int NOT NULL\n)\n'
                                elif attribute_type == 'simple':
                                    new_script += attribute_name + ' STRING(20)\n)\n'
                                else:
                                    for attr in composite_attrs:
                                        new_script += attr + ' STRING(20)\n'
                                    new_script += ')\n'
                                script['script'] = old_script + new_script
                    
            else:
                if annotation['text'].startswith('ADD SPECIALIZATION'):
                    spec_name = find_between(annotation['text'], 'SPECIALIZATION', 'FROM')
                    superclass = find_between(annotation['text'], 'FROM SUPERCLASS', 'TO')
                    superclass_entity = find_between(superclass, '- E ', ')')
                    subclass = find_between(annotation['text'], 'TO SUBCLASS', 'DISJOINTNESS')
                    subclass_entity = find_between(subclass, '- E ', ')')
                    disjoint = find_between(annotation['text'], 'DISJOINTNESS', ';')
                    for table in tables:
                        if table['name'] == superclass_entity:
                            primary_key = table['key']
                            break
                    for script in sql_script:
                        new_script = ''
                        if script['table'] == subclass_entity:
                            old_script = script['script']
                            if primary_key not in old_script:
                                old_script = old_script[:-3]+',\n'
                                new_script += primary_key+' int,\n'
                                new_script += 'FOREIGN KEY ('+primary_key+') REFERENCES '+superclass_entity+'('+primary_key+')\n)\n'
                                script['script'] = old_script + new_script
                    
                else:
                    if annotation['text'].startswith('ADD CATEGORY'):
                        cat_name = find_between(annotation['text'], 'ADD CATEGORY', 'TO')
                        category = find_between(annotation['text'], 'TO CATEGORY', 'FROM')
                        category_entity = find_between(category, '- E ', ')')
                        superclass = find_between(annotation['text'], 'FROM SUPERCLASS', ';')
                        superclass_entity = find_between(superclass, '- E ', ')')
                        for table in tables:
                            if table['name'] == category_entity:
                                primary_key = table['key']
                                break
                        for script in sql_script:
                            new_script = ''
                            if script['table'] == superclass_entity:
                                old_script = script['script']
                                if primary_key not in old_script:
                                    old_script = old_script[:-3]+',\n'
                                    new_script += primary_key+' int,\n'
                                    new_script += 'FOREIGN KEY ('+primary_key+') REFERENCES '+category_entity+'('+primary_key+')\n)\n'
                                    script['script'] = old_script + new_script

    return sql_script, tables

def generate_new_sql_script(checked, dbfd):
    diagrams = []
    annotation_list = []
    for check in checked:
        module = Module.objects.get(name=check, dbfd=dbfd)
        if not module.abstract:
            eer_diagram = EerDiagram.objects.get(module_id=module)
            diagrams.append({'eer': eer_diagram})
        relations = Relation.objects.filter(initial_module=module)
        for relation in relations:
            if relation.final_module.name in checked:
                annotations = RelationAnnotation.objects.filter(relation=relation)
                for annotation in annotations:
                    annotation_list.append({'text': annotation.text})
        constraints = Constraint.objects.filter(initial_module=module)
        for constraint in constraints:
            if constraint.final_module.name in checked:
                annotations = ConstraintAnnotation.objects.filter(constraint=constraint)
                for annotation in annotations:
                    annotation_list.append({'text': annotation.text})

    removed_attributes = []
    for annotation in annotation_list:
        if 'DROP ATTRIBUTE' in annotation['text']:
            attribute = find_between(annotation['text'], 'DROP ATTRIBUTE', ';')
            removed_attributes.append(attribute)

    sql_script, tables = generate_create_table_script(diagrams)

    sql_script, tables = generate_annotations_script(annotation_list, sql_script, tables)

    old_config_files = Configuration.objects.filter(dbfd=dbfd)
    for config_file in old_config_files:
        config_file.delete()

    file_name = 'script-'+time.strftime("%d-%m-%Y %H:%M:%S")+'.sql'
    destination = open('media/documents/sql.txt', 'wb+')
    for script in sql_script:
        destination.write(script['script'])
    configuration_file = Configuration()
    configuration_file.selected_modules = ''.join(e+', ' for e in checked)[:-2]
    configuration_file.dbfd = dbfd
    configuration_file.docfile.save(file_name, File(destination))
    destination.close()
    return file_name

def generate_update_sql_script(checked, dbfd):
    old_scripts = Configuration.objects.filter(dbfd=dbfd)
    if not old_scripts:
        return generate_new_sql_script(checked, dbfd)
    else:
        old_script_name = old_scripts[0].docfile.name
    new_script_name = generate_new_sql_script(checked, dbfd).replace(" ","_").replace(":","")

    import os
    os.system("diff -u " +"media/"+ str(old_script_name) + " media/documents/"+str(new_script_name)+">> media/documents/diff-"+time.strftime("%d-%m-%Y %H:%M:%S")+".sql")
    
    import difflib
    diff=difflib.ndiff(open('media/'+str(old_script_name)).readlines(), open('media/documents/'+str(new_script_name)).readlines())
    diff_script_name = "diff-"+str(old_script_name).replace("documents/","").replace(".sql","")+"--"+str(new_script_name).replace(".sql","")+".sql"
    dest = open('media/documents/'+diff_script_name, 'wb+')
    try:
        while 1:
            dest.write(diff.next())
    except:
        pass
    dest.close()
    return diff_script_name

def download(request,file_name):
    file_path = settings.MEDIA_ROOT +'/'+ file_name
    file_wrapper = FileWrapper(file(file_path,'rb'))
    file_mimetype = mimetypes.guess_type(file_path)
    response = HttpResponse(file_wrapper, content_type=file_mimetype )
    response['X-Sendfile'] = file_path
    response['Content-Length'] = os.stat(file_path).st_size
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str(file_name) 
    return response

@login_required
def update_diagram(request, template_name='update_diagram.html'):
    return render(request, template_name)