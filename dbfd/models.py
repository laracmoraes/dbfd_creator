from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver

class DBFD(models.Model):
	name = models.CharField(max_length=50, unique=True)

class Module(models.Model):
	name = models.CharField(max_length=200)
	mandatory = models.BooleanField(default=True)
	abstract = models.BooleanField(default=False)
	parent = models.ForeignKey('self', related_name='parent_module', null=True)
	dbfd = models.ForeignKey(DBFD, related_name='dbfd', null=False)
	
	def __str__(self):              # __unicode__ on Python 2
		return self.name

class RelationType(models.Model):
	rtype = models.CharField(max_length=50)

	def __str__(self):              # __unicode__ on Python 2
		return self.rtype

class Relation(models.Model):
	label = models.CharField(max_length=100)
	initial_module = models.ForeignKey(Module, related_name='module_initial')
	final_module = models.ForeignKey(Module, related_name='module_final')
	relation_type = models.ForeignKey(RelationType)
	dbfd = models.ForeignKey(DBFD, related_name='relation_dbfd', null=False)

	def __str__(self):              # __unicode__ on Python 2
		return self.label

class ConstraintType(models.Model):
	ctype = models.CharField(max_length=50)

	def __str__(self):              # __unicode__ on Python 2
		return self.ctype

class Constraint(models.Model):
	label = models.CharField(max_length=100, null=True)
	initial_module = models.ForeignKey(Module, related_name='moduleA')
	final_module = models.ForeignKey(Module, related_name='moduleB')
	constraint_type = models.ForeignKey(ConstraintType)
	dbfd = models.ForeignKey(DBFD, related_name='constraint_dbfd', null=False)

	def __str__(self):              # __unicode__ on Python 2
		return self.label

class RelationAnnotation(models.Model):
	text = models.CharField(null=False, max_length=300)
	relation = models.ForeignKey(Relation, blank=True)

class ConstraintAnnotation(models.Model):
	text = models.CharField(null=False, max_length=300)
	constraint = models.ForeignKey(Constraint, blank=True)
		
class Document(models.Model):
    docfile = models.FileField(upload_to='documents')
    dbfd = models.ForeignKey(DBFD, related_name='document_dbfd', null=False)

class Configuration(models.Model):
    docfile = models.FileField(upload_to='documents')
    selected_modules = models.TextField(null=False)
    dbfd = models.ForeignKey(DBFD, related_name='configuration_dbfd', null=False)

class EerDiagram(models.Model):
	module_id = models.ForeignKey(Module, null=False, default='')
	docfile = models.FileField(upload_to='documents')

class Cardinality(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class Participation(models.Model):
	name = models.CharField(max_length=50)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class EntityType(models.Model):
	name = models.CharField(max_length=100)
	weak = models.BooleanField(default=False)
	eer_diagram = models.ForeignKey(EerDiagram, blank=False)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class RelationshipType(models.Model):
	name = models.CharField(max_length=100)
	initial_entity = models.ForeignKey(EntityType, related_name='entity_initial')
	final_entity = models.ForeignKey(EntityType, related_name='entity_final')
	cardinality = models.ForeignKey(Cardinality)
	participation = models.ForeignKey(Participation)
	eer_diagram = models.ForeignKey(EerDiagram, blank=False)
	is_identifier = models.BooleanField(default=False)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class AssociativeEntity(models.Model):
	name = models.CharField(max_length=100)
	relationship = models.ForeignKey(RelationshipType, null=False)
	eer_diagram = models.ForeignKey(EerDiagram, blank=False, default=0)

class EntityAttribute(models.Model):
	name = models.CharField(max_length=100)
	attribute_type = models.CharField(max_length=100)
	entity_type = models.ForeignKey(EntityType, blank=False)
	data_type = models.CharField(max_length=30, default='STRING')
	is_null = models.BooleanField(default=False)
	is_unique = models.BooleanField(default=False)
	size = models.IntegerField(default=20)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class RelationshipAttribute(models.Model):
	name = models.CharField(max_length=100)
	attribute_type = models.CharField(max_length=100)
	relationship_type = models.ForeignKey(RelationshipType, blank=False)
	data_type = models.CharField(max_length=30, default='STRING')
	is_null = models.BooleanField(default=False)
	is_unique = models.BooleanField(default=False)
	size = models.IntegerField(default=20)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class CompositeAttribute(models.Model):
	name = models.CharField(max_length=100)
	attribute_type = models.CharField(max_length=100)
	entity_attribute = models.ForeignKey(EntityAttribute, blank=False)
	data_type = models.CharField(max_length=30, default='STRING')
	is_null = models.BooleanField(default=False)
	is_unique = models.BooleanField(default=False)
	size = models.IntegerField(default=20)

	def __str__(self):              # __unicode__ on Python 2
		return self.name
		
class Specialization(models.Model):
	name = models.CharField(max_length=100)
	superclass = models.ForeignKey(EntityType, related_name='superclass')
	# final_entity = models.ForeignKey(EntityType, related_name='subclass')
	disjoint = models.BooleanField(default=True)
	participation = models.ForeignKey(Participation, null=True)
	eer_diagram = models.ForeignKey(EerDiagram, blank=False)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class DirectInheritance(models.Model):
	source = models.ForeignKey(EntityType, null=False, related_name='source')
	target = models.ForeignKey(EntityType, null=False, related_name='target')
	eer_diagram = models.ForeignKey(EerDiagram, blank=False, default=0)

class Category(models.Model):
	name = models.CharField(max_length=100)
	# initial_entity = models.ForeignKey(EntityType, related_name='subclassA')
	category = models.ForeignKey(EntityType, related_name='category')
	eer_diagram = models.ForeignKey(EerDiagram, blank=False)
	# participation = models.ForeignKey(Participation, null=True)

	def __str__(self):              # __unicode__ on Python 2
		return self.name

class Subclass(models.Model):
	entity_type = models.ForeignKey(EntityType, null=False, related_name='subclass')
	specialization = models.ForeignKey(Specialization, null=False)

class Superclass(models.Model):
	entity_type = models.ForeignKey(EntityType, null=False, related_name='superclasses')
	category = models.ForeignKey(Category, null=False)
	participation = models.ForeignKey(Participation, null=True)

@receiver(post_delete, sender=EerDiagram)
def diagram_post_delete_handler(sender, **kwargs):
    diagram = kwargs['instance']
    storage, path = diagram.docfile.storage, diagram.docfile.path
    storage.delete(path)

@receiver(post_delete, sender=Configuration)
def diagram_post_delete_handler(sender, **kwargs):
    config = kwargs['instance']
    storage, path = config.docfile.storage, config.docfile.path
    storage.delete(path)

@receiver(post_delete, sender=Document)
def diagram_post_delete_handler(sender, **kwargs):
    document = kwargs['instance']
    storage, path = document.docfile.storage, document.docfile.path
    storage.delete(path)